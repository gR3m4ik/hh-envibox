import { createStore } from 'vuex';

export default createStore({
    state: {
        formData: []
    },
    mutations: {
        addFormData(state, formData) {
            state.formData.push(formData);
        },
        clearFormData(state) {
            state.formData = [];
        }
    },
    actions: {
        saveFormData({ commit }, formData) {
            commit('addFormData', formData);
        },
        clearForm({ commit }) {
            commit('clearFormData');
        }
    },
    getters: {
        formDataArray: state => state.formData
    }
});
