import { createWebHistory, createRouter } from "vue-router";
import Form from "@/views/Form.vue";
import Result from "@/views/Result.vue";
import Error404 from "@/views/Error404.vue";

const routes = [
    {
        path: "/",
        name: "Form",
        component: Form,
    },
    {
        path: "/result-form",
        name: "Result with vue store",
        component: Result,
    },
    {
        path: "/result-db-form",
        name: "Result with db",
        component: Result,
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'Error404',
        component: Error404,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
