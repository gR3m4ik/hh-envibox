import axios from 'axios';
axios.defaults.withCredentials = true

export default {
    data() {
        return {
            name: '',
            email: '',
            body: ''
        };
    },
    methods: {
        submitForm() {
            // проверяю, ввел ли поля
            if (!this.name || !this.email || !this.body) {
                if (!this.name) {
                    this.setFocus('name');
                } else if (!this.email) {
                    this.setFocus('email');
                } else {
                    this.setFocus('body');
                }
                return;
            }

            // Сохраняю в store
            this.$store.dispatch('saveFormData', {
                name: this.name,
                email: this.email,
                body: this.body,
                date: new Date().toISOString() // для себя
            });
            // console.log('Stored form data:', this.$store.getters.formDataArray);

            // Отправляем данные на сервер
            axios.post('/api/form/feedback', {
                    name: this.name,
                    email: this.email,
                    body: this.body
                })
                .then(response => {
                    console.log('Ответ от сервера:', response.data);
                })
                .catch(error => {
                    console.error('Ошибка:', error);
                });


            this.name = '';
            this.email = '';
            this.body = '';
        },
        clearErrorMessage(field) {
            if (field === 'name') {
                this.name = '';
            } else if (field === 'email') {
                this.email = '';
            } else if (field === 'body') {
                this.body = '';
            }
        },
        setFocus(field) {
            const inputField = document.getElementById(field);
            if (inputField) {
                inputField.focus();
            }
        }
    }
};


// import axios from 'axios'
// axios.defaults.withCredentials = true
//
// import {ref} from "vue";
//
// export default {
//     name: 'Hello',
//
//     setup() {
//
//         const migrations = ref([])
//
//         function onSuccess(response) {
//             migrations.value = response.data;
//         }
//
//         function load() {
//             axios.get('/api/migration')
//                 .then(onSuccess)
//                 .catch((error) => { alert(`Error ${error.message}`) })
//         }
//
//         return {
//             load,
//             migrations
//         }
//     }
// }