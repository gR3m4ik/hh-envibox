<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Feedback;

class FeedbackFactory extends Factory
{
    protected $model = Feedback::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'body' => $this->faker->paragraph,
        ];
    }

    /**
     * Save data to db or send to some mails
     *
     * @param $data - request data
     * @param $state - state for database save data or send to mail data
     * @return null
     */
    public function save($data, $state = null)
    {
        if ($state === 'email')
            return $this->sendByEmail($data);
        elseif ($state === 'database')
            return $this->saveToDatabase($data);

        return null;
    }

    protected function saveToDatabase($data)
    {
        $model = new $this->model;
        $model->fill($data);
        $model->save();

        return 'Данные сохранены в базу';
    }

    protected function sendByEmail($data)
    {
        // Как пример отправка на почту...
        // Mail::to($data['email'])->send(new FeedbackMail($data));
        return 'Данные отправлены на почту';
    }
}
