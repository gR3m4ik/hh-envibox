<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Database\Factories\FeedbackFactory;

class FeedbackController extends Controller
{
    protected $feedbackFactory;

    public function __construct(FeedbackFactory $feedbackFactory)
    {
        $this->feedbackFactory = $feedbackFactory;
    }

    public function saveFormData(Request $request)
    {
        // Валидация данных
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'body' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        // Создание записи с использованием фабрики FeedbackFactory
        $statusRecord = $this->feedbackFactory->save([
            'name' => $request->name,
            'email' => $request->email,
            'body' => $request->body
        ], 'database');

        if ($statusRecord) {
            return response()->json(['message' => $statusRecord], 200);
        } else {
            return response()->json(['message' => 'Ошибка сохранения данных'], 500);
        }
    }
}
